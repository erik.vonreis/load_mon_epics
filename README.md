# load_mon_epics

computer load monitoring over EPICS

 * `load_mon_epics.py` pcaspy-based service monitor EPICS soft IOC
 * `load_mon_epics.service` systemd service unit
